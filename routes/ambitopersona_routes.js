//Requires
var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

const { tokenVerify } = require('../middlewares/autenticacion');
const cors = require('cors');
//Init variables
var express_var = express();
const bodyParser = require('body-parser');

var ambitopersonaService = require('../services/ambitopersona_service');
var asignacionService = require('../services/asignacion_service');

//middleware para habilitar el CORS
express_var.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,token");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
  });

//middleware: se ejecuta antes de la petición
express_var.use(bodyParser.urlencoded({extend:false}));
express_var.use(bodyParser.json());


  //:::::::::::::::::::::::::::::::::::::::::::::::
  //:::::Métodos para Ambitopersona::::::
  //:::::::::::::::::::::::::::::::::::::::::::::::


express_var.get('/ambitopersona/:codigo_ambito', (req, res) => {
  console.log('Atendiendo GET /ambitopersona/:codigo_ambito');
  ambitopersonaService.listarPorAmbito(req, res).then(response =>{
    var salidaArray = new Array();
    var count = 0;
    for(var myobject in response)
    {
      if (response[myobject]["ambito"]!=null) {
        count++;
        var jsonData = {}
        ambito = response[myobject]["ambito"]["descripcion_ambito"]
        peso = response[myobject]["peso"]
        registro = response[myobject]["persona"]["registro"]
        nombres = response[myobject]["persona"]["nombres"]
        jsonData['ambito'] = ambito
        jsonData['registro'] = registro
        jsonData['nombres'] = nombres
        jsonData['apellido_paterno'] = response[myobject]["persona"]["apellido_paterno"]
        jsonData['apellido_materno'] = response[myobject]["persona"]["apellido_materno"]
        jsonData['unidad'] = response[myobject]["persona"]["unidad"]
        jsonData['disciplina'] = response[myobject]["persona"]["disciplina"]
        jsonData['building_block'] = response[myobject]["persona"]["building_block"]
        jsonData['peso'] = peso
        jsonData['cantidad'] = count;
        salidaArray.push(jsonData);
      }
    }
    salidaArray.sort(function(a,b){
      if(b[peso] > a[peso]) {return 1;}
      if(a[peso] > b[peso]) {return -1;}
      return 0;
    });
    res.json(salidaArray);
  }).catch( error => {
      res.status( 500 ).send( error )
  });
});

express_var.post('/ambitoxpersona', (req, res) => {
  console.log('Atendiendo GET /ambitopersona');
  ambitopersonaService.listarPersonaAmbito(req, res).then(response =>{
  console.log(response);
  res.json(response);
  }).catch( error => {
    res.status( 500 ).send( error )
  });
});

//retorna la lista de personas pertenecientes a un building_block,disciplina, con su
//respectiva experiencia en un ambito.
//@param: req.body.building_block
//@param: req.body.disciplina
//@param: req.body.ambito
express_var.post('/ambitoxpersonaxfte', (req, res) => {
  console.log('Atendiendo GET /ambitopersonaxfte '+req.body.building_block+req.body.disciplina);
  Promise.all( [ambitopersonaService.listarPersonaAmbito(req, res),
              asignacionService.sumadefteall(req, res)])
              .then(response => {
                if(response[0].length==0){
                  persona = "No se encontraron sugeridos para " + req.body.disciplina;
                  console.log(persona);
                }
                console.log(response[0]);
                for(var iterador in response[0])
                {
                  response[0][iterador]['ftedisponible'] = 1;
                  for(var iterador2 in response[1]){
                    console.log(response[0][iterador]['_id']['registro']);
                    console.log(response[1][iterador2]['_id']['registro']);
                    if(response[0][iterador]['_id']['registro'] ==
                        response[1][iterador2]['_id']['registro'] )
                    {
                      console.log("match");
                      response[0][iterador]['ftedisponible'] = 1 - response[1][iterador2]['ftes'];
                    }
                  }
                  iterador2 = 0;
                }
                //ordenamiento de resultados, por pesos.
                response[0].sort(function(a,b){
                  if(b.ambitos[0].peso > a.ambitos[0].peso){return 1;}
                  if(a.ambitos[0].peso > b.ambitos[0].peso ){return -1;}
                  return 0;
                });
                res.json(response[0]);
              });
});

express_var.get('/ambitopersonas', (req, res) => {
  console.log('Atendiendo GET /ambitopersonas');
  ambitopersonaService.listarCountAmbitos(req, res).then(response =>{
  res.json(response);
  }).catch( error => {
    res.status( 500 ).send( error )
  });
});

express_var.get('/ambitosdepersona/:registro', (req, res) => {
  console.log('Atendiendo GET /ambitosdepersona');
  ambitopersonaService.listarAmbitosRegistro(req, res).then(response =>{
  res.json(response);
  }).catch( error => {
    res.status( 500 ).send( error )
  });
});


module.exports = express_var;
