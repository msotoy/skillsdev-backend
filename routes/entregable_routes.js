//Requires
var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

const { tokenVerify } = require('../middlewares/autenticacion');
const cors = require('cors');
//Init variables
var express_var = express();
const bodyParser = require('body-parser');

var entregableService = require('../services/entregable_service');

//middleware para habilitar el CORS
express_var.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,token");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
  });

//middleware: se ejecuta antes de la petición
express_var.use(bodyParser.urlencoded({extend:false}));
express_var.use(bodyParser.json());


  //:::::::::::::::::::::::::::::::::::::::::::::::
  //:::::Métodos para Entregable::::::
  //:::::::::::::::::::::::::::::::::::::::::::::::


  // Lista todos los entregables
  express_var.get('/entregable', (req, res) => {
    console.log('Atendiendo GET /entregable');
    entregableService.listar(req, res).then(response =>{
    console.log(response);
    res.json(response);
    }).catch( error => {
      res.status( 500 ).send( error )
    });
  });

  //Lista de entregables por sdaTool de proyecto
  express_var.get('/proyecto/:sdatool/entregable', (req, res) => {
    console.log('Atendiendo GET /proyecto/:sdatool/entregable');
    var sdatool = req.params.sdatool;
    console.log('sdatool:'+sdatool);
      entregableService.listarPorCodigoProyecto(req, res).then(response =>{
          res.json(response);
      }).catch( error => {
          res.status( 500 ).send( error )
        });
  });

  // Cantidad de entregables
  // express_var.get('/entregable/count/', (req, res) => {
  //   console.log('Atendiendo GET /entregable/count');
  //   entregableService.contador(req, res).then(response =>{
  //   console.log(response);
  //   res.json(response);
  //   }).catch( error => {
  //     res.status( 500 ).send( error )
  //   });
  // });

//Crea nuevo entregable
express_var.post('/entregable',(req, res)=>{
  console.log('Atendiendo POST /entregable');
    entregableService.contador(req, res).then(response =>{
      var id = response;
      var body = req.body;
      entregableService.crear(body,id,req, res).then(response =>{
        res.json(response);
      }).catch( error => {
        res.status( 500 ).send( error )
        });
    }).catch( error => {
      res.status( 500 ).send( error )
    });

});

//Actualiza entregable
express_var.put('/entregable/:id',(req,res)=>{
  console.log('Atendiendo PUT /entregable/:id');
  var body = req.body;
  console.log(body);
  entregableService.actualizar(body,req, res).then(response =>{
      res.json(response);
  }).catch( error => {
      res.status( 500 ).send( error )
    });

});

//Eliminacion(logica) entregable
express_var.delete('/entregable/:id',(req,res)=>{
  console.log('Atendiendo DELETE /entregable/:id');
  var body = req.body;
  entregableService.eliminar(body,req, res).then(response =>{
      res.json(response);
  }).catch( error => {
      res.status( 500 ).send( error )
    });

});
module.exports = express_var;
