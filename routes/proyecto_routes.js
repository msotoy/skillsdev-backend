//Requires
var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

const { tokenVerify } = require('../middlewares/autenticacion');
const cors = require('cors');
//Init variables
var express_var = express();
const bodyParser = require('body-parser');

var proyectoService = require('../services/proyecto_service');

//middleware para habilitar el CORS
express_var.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,token");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
  });

//middleware: se ejecuta antes de la petición
express_var.use(bodyParser.urlencoded({extend:false}));
express_var.use(bodyParser.json());


  //:::::::::::::::::::::::::::::::::::::::::::::::
  //:::::Métodos para Proyecto::::::
  //:::::::::::::::::::::::::::::::::::::::::::::::


  // Lista todos los proyectos
  express_var.get('/proyecto', (req, res) => {
    console.log('Atendiendo GET /proyecto');
    proyectoService.listarConPeriodo(req, res).then(response =>{
    console.log(response);
    res.json(response);
    }).catch( error => {
      res.status( 500 ).send( error )
    });
  });

  //Lista proyecto por descripcion periodo
  express_var.get('/proyecto/periodo/:descripcion', (req, res) => {
    console.log('Atendiendo GET /proyecto/periodo/:descripcion');
    var descripcion = req.params.descripcion;
    console.log('descripcion:'+descripcion);
      proyectoService.listarPorDescripcion(req, res).then(response =>{
          res.json(response);
      }).catch( error => {
          res.status( 500 ).send( error )
        });
  });

  //Lista proyecto por sda
  express_var.get('/proyecto/:sdatool', (req, res) => {
    console.log('Atendiendo GET /proyecto/:sdatool');
    var sdatool = req.params.sdatool;
    console.log('sdatool:'+sdatool);
      proyectoService.listarPorSdatool(req, res).then(response =>{
          res.json(response);
      }).catch( error => {
          res.status( 500 ).send( error )
        });

  });

  //Lista proyecto por sda con entregables
  express_var.get('/proyecto/:sdatool/entregables', (req, res) => {
    console.log('Atendiendo GET /proyecto/:sdatool');
    var sdatool = req.params.sdatool;
    console.log('sdatool:'+sdatool);
      proyectoService.listarPorSdatool(req, res).then(response =>{
          res.json(response);
      }).catch( error => {
          res.status( 500 ).send( error )
        });

  });

  // Cantidad de proyectos
  // express_var.get('/proyecto/count/', (req, res) => {
  //   console.log('Atendiendo GET /proyecto/count');
  //   proyectoService.contador(req, res).then(response =>{
  //   console.log(response);
  //   res.json(response);
  //   }).catch( error => {
  //     res.status( 500 ).send( error )
  //   });
  // });

//Crea nuevo proyecto
express_var.post('/proyecto',(req, res)=>{
  console.log('Atendiendo POST /proyecto');
    proyectoService.contador(req, res).then(response =>{
      var id = response;
      var body = req.body;
      proyectoService.crear(body,id,req, res).then(response =>{
        res.json(response);
      }).catch( error => {
        res.status( 500 ).send( error )
        });
    }).catch( error => {
      res.status( 500 ).send( error )
    });

});

//Actualiza proyecto
express_var.put('/proyecto/:id',(req,res)=>{
  console.log('Atendiendo PUT /proyecto/:id');
  var body = req.body;
  console.log(body);
  proyectoService.actualizar(body,req, res).then(response =>{
      res.json(response);
  }).catch( error => {
      res.status( 500 ).send( error )
    });

});

//Eliminacion(logica) proyecto
express_var.delete('/proyecto/:id',(req,res)=>{
  console.log('Atendiendo DELETE /proyecto/:id');
  var body = req.body;
  proyectoService.eliminar(body,req, res).then(response =>{
      res.json(response);
  }).catch( error => {
      res.status( 500 ).send( error )
    });

});
module.exports = express_var;
