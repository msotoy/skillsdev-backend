//Requires
var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

const { tokenVerify } = require('../middlewares/autenticacion');
const cors = require('cors');
//Init variables
var express_var = express();
const bodyParser = require('body-parser');

var periodoService = require('../services/periodo_service');

//middleware para habilitar el CORS
express_var.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,token");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
  });

//middleware: se ejecuta antes de la petición
express_var.use(bodyParser.urlencoded({extend:false}));
express_var.use(bodyParser.json());


  //:::::::::::::::::::::::::::::::::::::::::::::::
  //:::::Métodos para Periodo::::::
  //:::::::::::::::::::::::::::::::::::::::::::::::


  // Lista todos los periodos
  express_var.get('/periodo', (req, res) => {
    console.log('Atendiendo GET /periodo');
    periodoService.listar(req, res).then(response =>{
    console.log(response);
    res.json(response);
    }).catch( error => {
      res.status( 500 ).send( error )
    });
  });

  //Lista periodo por descripcion
  express_var.get('/periodo/:descripcion', (req, res) => {
    console.log('Atendiendo GET /periodo/:descripcion');
    var descripcion = req.params.descripcion;
    console.log('descripcion:'+descripcion);
      periodoService.listarPorDescripcion(req, res).then(response =>{
          res.json(response);
      }).catch( error => {
          res.status( 500 ).send( error )
        });

  });

  // Cantidad de periodos
  // express_var.get('/periodo/count/', (req, res) => {
  //   console.log('Atendiendo GET /periodo/count');
  //   periodoService.contador(req, res).then(response =>{
  //   console.log(response);
  //   res.json(response);
  //   }).catch( error => {
  //     res.status( 500 ).send( error )
  //   });
  // });

//Crea nuevo periodo
express_var.post('/periodo',(req, res)=>{
  console.log('Atendiendo POST /periodo');
    periodoService.contador(req, res).then(response =>{
      var id = response;
      var body = req.body;
      periodoService.crear(body,id,req, res).then(response =>{
        res.json(response);
      }).catch( error => {
        res.status( 500 ).send( error )
        });
    }).catch( error => {
      res.status( 500 ).send( error )
    });

});

//Actualiza periodo
express_var.put('/periodo/:id',(req,res)=>{
  console.log('Atendiendo PUT /periodo/:id');
  var body = req.body;
  console.log(body);
  periodoService.actualizar(body,req, res).then(response =>{
      res.json(response);
  }).catch( error => {
      res.status( 500 ).send( error )
    });

});

//Eliminacion(logica) periodo
express_var.delete('/periodo/:id',(req,res)=>{
  console.log('Atendiendo DELETE /periodo/:id');
  var body = req.body;
  periodoService.eliminar(body,req, res).then(response =>{
      res.json(response);
  }).catch( error => {
      res.status( 500 ).send( error )
    });

});
module.exports = express_var;
