//Requires
var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

const { tokenVerify } = require('../middlewares/autenticacion');
const cors = require('cors');
//Init variables
var express_var = express();
const bodyParser = require('body-parser');

var asignacionService = require('../services/asignacion_service');

//middleware para habilitar el CORS
express_var.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,token");
    res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
    next();
  });

//middleware: se ejecuta antes de la petición
express_var.use(bodyParser.urlencoded({extend:false}));
express_var.use(bodyParser.json());


  //:::::::::::::::::::::::::::::::::::::::::::::::
  //:::::Métodos para Asignacion::::::
  //:::::::::::::::::::::::::::::::::::::::::::::::


  // Lista todos los asignacions
  express_var.get('/asignacion', (req, res) => {
    console.log('Atendiendo GET /asignacion');
    asignacionService.listar(req, res).then(response =>{
    console.log(response);
    res.json(response);
    }).catch( error => {
      res.status( 500 ).send( error )
    });
  });

express_var.get('/asignacion/listarPorCodigoEntregable/:sdatool', (req, res) => {
  console.log('Atendiendo GET /asignacion/listarPorCodigoEntregable/:sdatool');
  asignacionService.listarPorCodigoEntregable(req, res).then(response =>{
  console.log(response);
  res.json(response);
  }).catch( error => {
    res.status( 500 ).send( error )
  });
});

//Lista asignacion por registro
express_var.get('/asignacionesxresgistro/:registro', (req, res) => {
  console.log('Atendiendo GET /asignacion/:registro');
  var registro = req.params.registro;
  console.log('registro:'+registro);
  asignacionService.listarPorPersona(req, res).then(response =>{
        res.json(response);
    }).catch( error => {
        res.status( 500 ).send( error )
      });
});
  //Lista asignacion por registro
  express_var.get('/asignacion/:registro', (req, res) => {
    console.log('Atendiendo GET /asignacion/:registro');
    var registro = req.params.registro;
    console.log('registro:'+registro);
    asignacionService.listarPorPersona(req, res).then(response =>{
          res.json(response);
      }).catch( error => {
          res.status( 500 ).send( error )
        });
  });

  // Cantidad de asignacions
  // express_var.get('/asignacion/count/', (req, res) => {
  //   console.log('Atendiendo GET /asignacion/count');
  //   asignacionService.contador(req, res).then(response =>{
  //   console.log(response);
  //   res.json(response);
  //   }).catch( error => {
  //     res.status( 500 ).send( error )
  //   });
  // });

//Crea nuevo asignacion
express_var.post('/asignacion',(req, res)=>{
  console.log('Atendiendo POST /asignacion');
    asignacionService.contador(req, res).then(response =>{
      var id = response;
      var body = req.body;
      asignacionService.crear(body,id,req, res).then(response =>{
        res.json(response);
      }).catch( error => {
        res.status( 500 ).send( error )
        });
    }).catch( error => {
      res.status( 500 ).send( error )
    });

});

//Actualiza asignacion
express_var.put('/asignacion',(req,res)=>{
  console.log('Atendiendo PUT /asignacion');
  asignacionService.actualizar(req, res).then(response =>{
  console.log(response);
      res.json(response);
  }).catch( error => {
      res.status( 500 ).send( error )
    });

});

//Eliminacion(logica) asignacion
express_var.delete('/asignacion/:id',(req,res)=>{
  console.log('Atendiendo DELETE /asignacion/:id');
  var body = req.body;
  asignacionService.eliminar(body,req, res).then(response =>{
      res.json(response);
  }).catch( error => {
      res.status( 500 ).send( error )
    });

});
//Demanda
express_var.get('/asignacion/listarxambito/:codigo_ambito', (req, res) => {
  console.log('/asignacion/listarxambito/:codigo_ambito');
  asignacionService.listardemanda(req, res).then(response =>{
    var salidaArray = new Array();
    var count = 0;
    console.log(response.length);
        console.log(response);
    for(var myobject in response)
    {
      if (response[myobject]["ambito"]!=null) {
        var jsonData = {}
         ambito = response[myobject]["ambito"]["descripcion_ambito"]
         tipo = response[myobject]["tipo"]
         fte = response[myobject]["fte"]
         if( response[myobject]["entregable"]!=null){
           entregable = response[myobject]["entregable"]["descripcion"]
           if( response[myobject]["entregable"]["proyecto"]!=null){
             cod_proyecto = response[myobject]["entregable"]["proyecto"]["sdatool"]
           }
           if( response[myobject]["entregable"]["proyecto"]!=null){
             des_proyecto = response[myobject]["entregable"]["proyecto"]["descripcion"]
           }
         }
         count++;
         jsonData['ambito'] = ambito;
         jsonData['tipo'] = tipo;
         jsonData['fte'] = fte;
         jsonData['entregable'] = entregable;
         jsonData['cod_proyecto'] = cod_proyecto;
         jsonData['des_proyecto'] = des_proyecto;
         jsonData['cantidad'] = count;
        salidaArray.push(jsonData);
      }
    }
    res.json(salidaArray);
  }).catch( error => {
      res.status( 500 ).send( error )
  });
});

express_var.get('/listarxambitos', (req, res) => {
  console.log('Atendiendo GET /listarxambitos ');
  asignacionService.listarCountAmbitos(req, res).then(response =>{
  res.json(response);
  }).catch( error => {
    res.status( 500 ).send( error )
  });
});

module.exports = express_var;
