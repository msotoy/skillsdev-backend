var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

const { tokenVerify } = require('../middlewares/autenticacion');
const cors = require('cors');
//Init variables
var express_var = express();
const bodyParser = require('body-parser');

var ninjaprojectService = require('../services/ninjaproject_service');

//middleware para habilitar el CORS
express_var.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,token");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS');
  next();
});

//middleware: se ejecuta antes de la petición
express_var.use(bodyParser.urlencoded({extend:false}));
express_var.use(bodyParser.json());

express_var.use(cors());
express_var.options('*', cors());

//:::::::::::::::::::::::::::::::::::::::::::::::
//::::::Métodos para ninjaproject::::::
//:::::::::::::::::::::::::::::::::::::::::::::::

// retorna informacion ninja
express_var.get('/ninja/:registro', (req, res) => {
  console.log('Atendiendo GET /NINJA ' + req.params.registro);
    ninjaprojectService.listarPorRegistro(req, res).then(response =>{
        if(response[0]["informacion"].length>0&&response[0]["informacion"][0]["Tech u!"])
          response[0]["niveltechu"] = response[0]["informacion"][0]["Tech u!"];
        response[0]["puntos"] = response[0]["puntos"]*1000;
        res.json(response);
    }).catch( error => {
        res.status( 500 ).send( error )
      });
});

module.exports = express_var;
