//Requires
require('dotenv').config();
var express = require('express');

//Init variables
var express_var = express();
const cors = require('cors');


var ninjaprojectRoutes = require('./routes/ninjaproject_routes');
var periodoRoutes = require ('./routes/periodo_routes');
var proyectoRoutes = require ('./routes/proyecto_routes');
var entregableRoutes = require ('./routes/entregable_routes');
var personaRoutes = require('./routes/persona_routes');
var usuarioRoutes = require('./routes/usuario_routes');
var skillRoutes = require('./routes/skill_routes');

var asignacionRoutes = require ('./routes/asignacion_routes');
var ambitopersonaRoutes = require ('./routes/ambitopersona_routes');

//express_var.use(cors);
//express_var.options('*', cors());

//Rutas
express_var.use('/',ninjaprojectRoutes);
express_var.use('/',usuarioRoutes);
express_var.use('/',personaRoutes);
express_var.use('/',skillRoutes);

express_var.use('/',periodoRoutes);
express_var.use('/',proyectoRoutes);
express_var.use('/',entregableRoutes);
express_var.use('/',asignacionRoutes);
express_var.use('/',ambitopersonaRoutes);


//Listenning requests
var port = process.env.PORT || 3000;
express_var.listen(port,()=>{
    console.log('Express server puerto '+port);
});

module.exports = {
    express_var
}
