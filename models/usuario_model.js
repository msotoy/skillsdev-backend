var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var persona = mongoose.model('persona');


var usuarioSchema = new Schema({
  registro:{type: String, require: [true, 'El registro es necesario']},
  password:{type: String, require:[true, 'El password es necesario']},
  id_persona:{ type: Schema.ObjectId, ref: 'persona' }



});

module.exports = mongoose.model('usuario',usuarioSchema);
