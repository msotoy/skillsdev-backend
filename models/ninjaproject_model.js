var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');

var Schema = mongoose.Schema;

var ninjaprojectSchema = new Schema({
  registro : {type: String},
  puntos : {type: Number},
  cinturon : {type: String},
  email : {type: String},
  niveltechu:{type: String},
  cargo:{type: String},
  informacion:{type: Array}
});

ninjaprojectSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('ninjaproject',ninjaprojectSchema);
