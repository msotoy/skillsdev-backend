var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');

var Schema = mongoose.Schema;
var periodo = mongoose.model('periodo');

var proyectoSchema = new Schema({
  id:{type: Number},
  prioridad:{type: Number},
  dominio:{type: String},
  sdatool:{type: String},
  descripcion:{type: String},
  estado:{type: Number},
  periodo: {type: Schema.Types.ObjectId, ref: 'periodo'}
});

proyectoSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('proyecto',proyectoSchema);
