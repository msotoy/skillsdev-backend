var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');

var Schema = mongoose.Schema;
var proyecto = mongoose.model('proyecto');
 // var proyecto = require('../models/proyecto_model');

var entregableSchema = new Schema({
  id:{type: Number},
  codigo:{type: String},
  descripcion:{type: String},
  disciplina:{type: String},
  especialidad:{type: String},
  program_manager:{type: String},
  lider_disciplina:{type: String},
  lider_tecnico:{type: String},
  equipo_final:{type: String},
  tipo:{type: String},
  id_proyecto:{type: Number},
  proyecto: {type: Schema.Types.ObjectId, ref: 'proyecto'},
  estado:{type: Number}
});

entregableSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('entregable',entregableSchema);
