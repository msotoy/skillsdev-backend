var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var menus = mongoose.model('menus');


//Schema para perfilmenus

var perfilmenusSchema = new Schema({
//  id_menu:{type: Number, require: [true, 'El id es necesario']},
  id_menu:{ type: Schema.ObjectId, ref: 'menus' },
  id_perfil:{type: Number, require: [true, 'El id es necesario']},
  activo:{type: Boolean, require:[true, 'El estado es necesario']}
});


module.exports = mongoose.model('perfilmenus',perfilmenusSchema);
