var mongoose = require('mongoose');

var Schema = mongoose.Schema;

//Schema para perfil

var perfilesSchema = new Schema({
  id_perfil:{type: Number, require: [true, 'El id es necesario']},
  name_perfil:{type: String, require:[true, 'El nombre es necesario']},
  activo:{type: Boolean, require:[true, 'El estado es necesario']}
});



module.exports = mongoose.model('perfiles',perfilesSchema);
