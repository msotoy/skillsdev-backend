var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');

var Schema = mongoose.Schema;

var ambitopersonaSchema = new Schema({
  ambito:{type: Schema.Types.ObjectId, ref: 'ambito'},
  persona: {type: Schema.Types.ObjectId, ref: 'persona'},
  peso:{type: Number}
});

ambitopersonaSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('ambitopersona',ambitopersonaSchema);
