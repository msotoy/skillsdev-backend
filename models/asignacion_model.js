var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');
var entregable = mongoose.model('entregable');
var persona = mongoose.model('persona');
// var ambito = mongoose.model('ambito');
var Schema = mongoose.Schema;

var asignacionSchema = new Schema({
  ambito:{type: Schema.Types.ObjectId, ref: 'ambito'},
  tipo:{type: String},
  fte:{type: Number},
  comentario:{type: String},
  entregable: {type: Schema.Types.ObjectId, ref: 'entregable'},
  persona: {type: Schema.Types.ObjectId, ref: 'persona'},
  registro: {type: String}
});

asignacionSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('asignacion',asignacionSchema);
