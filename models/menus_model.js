var mongoose = require('mongoose');

var Schema = mongoose.Schema;



//Schema para menus

var MenusSchema = new Schema({
  id_menu:{type: Number, require: [true, 'El id es necesario']},
  name_menu:{type: String, require:[true, 'El nombre es necesario']},
  url_menu:{type: String, require:[true, 'El Url  es necesario']},
  activo:{type: Boolean, require:[true, 'El estado es necesario']}
});

module.exports = mongoose.model('menus',MenusSchema);
