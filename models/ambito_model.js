var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');

var Schema = mongoose.Schema;

var ambitoSchema = new Schema({
  codigo_ambito:{type: String},
  estado_ambito:{type: String},
  descripcion_ambito:{type: String}
});

ambitoSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('ambito',ambitoSchema);
