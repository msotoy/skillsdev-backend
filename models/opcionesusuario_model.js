var mongoose = require('mongoose');


var Schema = mongoose.Schema;


//var menus = require('/menus_model');
//var perfil = require('/perfiles_model');


//Schema para perfilesusuarios con objetos

var opcionesusuarioSchema = new Schema({
 _id:{type: Number, require: [true, 'El id es necesario']},
 id_perfil:{type: Number, require: [true, 'El id es necesario']},
 perfil:{ type: Schema.ObjectId, ref: "perfiles" } ,
 menus:{ type: Schema.ObjectId, ref: "menus" },
 activo:{type: Boolean, require:[true, 'El estado es necesario']}
});

module.exports = mongoose.model('opcionesusuario',opcionesusuarioSchema);
