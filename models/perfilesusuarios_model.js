var mongoose = require('mongoose');

var Schema = mongoose.Schema;


//Schema para perfilesusuarios

var perfilesusuariosSchema = new Schema({
  id_perfil:{type: Number, require: [true, 'El id es necesario']},
  registro:{type: String, require:[true, 'El nombre es necesario']},
  activo:{type: Boolean, require:[true, 'El estado es necesario']}
});

module.exports = mongoose.model('perfilusuarios',perfilesusuariosSchema);
