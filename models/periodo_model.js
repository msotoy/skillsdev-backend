var mongoose = require('mongoose');
var aggregatePaginate  = require('mongoose-aggregate-paginate-v2');

var Schema = mongoose.Schema;

var periodoSchema = new Schema({
  anio:{type: Number},
  descripcion:{type: String},
  fecha_inicio:{type: Date},
  fecha_fin:{type: Date},
  estado:{type: Number}
});

periodoSchema.plugin(aggregatePaginate);
module.exports = mongoose.model('periodo',periodoSchema);
