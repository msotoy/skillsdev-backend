var Asignacion = require('../models/asignacion_model');
var Entregable = require('../models/entregable_model');
var Ambito = require('../models/ambito_model');

exports.listarPorCodigoEntregable = function (req,res){
      var regex = new RegExp(req.params.sdatool, "i");
      return Asignacion.find({}).populate({path: 'ambito'}).populate({
      path: 'entregable',
      populate: {
        path : 'proyecto',
          match:{
         $or: [{ sdatool: {$regex:regex} }, { descripcion: {$regex:regex} }]},
        populate: {
          path : 'periodo'
        }
      }
    }).populate({path: 'persona'})
      .exec().catch(error => {
          res.status(500).send(error);
      });
}

exports.listarPorPeriodo = function (req,res){
      return Asignacion.find({}).populate({
      path: 'entregable',
      populate: {
        path : 'proyecto',
        populate: {
          path : 'periodo',
          match:{ descripcion: {$eq: req.params.descripcion}},
        }
      }
    }).populate({path: 'persona'})
      .exec().catch(error => {
          res.status(500).send(error);
      });
}

exports.listarPorPersona = function (req,res){
  var codigo=req.params.registro;
    console.log(codigo);

      return Asignacion.find(  {"registro":codigo}  ).populate({
      path: 'entregable',
      populate: {
        path : 'proyecto',
        populate: {
          path : 'periodo'
        }
      }
    }).populate({
      path: 'persona',
      match:{ registro: {$eq: codigo  }}
    })
      .exec().catch(error => {
          res.status(500).send(error);
      });
}

exports.listar = function (req,res){
      return Asignacion.find({}).populate({
      path: 'entregable',
      populate: {
        path : 'proyecto',
        populate: {
          path : 'periodo'
        }
      }
    }).populate({
      path: 'persona'
    })
      .exec().catch(error => {
          res.status(500).send(error);
      });
}

exports.contador = function (req,res){
  return Asignacion.find({}).then(
          asignaciones => {
    return asignaciones.length;
  }).catch(error => {
          res.status(500).send(error);
      });
}

exports.crear = function (body,id,req,res){
  var asignacion = new Asignacion({
    id: 			  id,
    perfil: 	body.perfil,
    tipo: 		body.tipo ,
    fte: 	  body.fte,
    comentario:body.comentario,
    id_proyecto: 	  body.id_proyecto,
    id_entregable: 	      body.id_entregable,
    registro_persona: 	      body.registro_persona,
    estado: 	      body.estado
  });

  return asignacion.save().then(
          asignacionCreado => asignacionCreado
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.actualizar = function (req,res){
  var query = {_id:req.body._id}
  console.log(req.body._id)
  return Asignacion.findOneAndUpdate(query,req.body,{useFindAndModify: false, new:true}).then(
          asignacionActualizado => asignacionActualizado
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.eliminar = function (body,req,res){
  var query = {id:body.id};
  body = {id:body.id,estado:'0'};
  return Asignacion.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          asignacionActualizado => asignacionActualizado
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.sumadefte = function (object, registro){
      console.log('suma de ftes');
      return Asignacion.aggregate([
          { $match:{
                "registro": registro
              }
          },{
            $group:
            {
              _id:'$registro',
              ftes: { $sum: "$fte"}
            }
          },{
            $addFields:{
              indice:object
            }
          }]
      ).exec().catch(error => {
          res.status(500).send(error);
      });
}

exports.listardemanda = function (req,res){
      var regex = new RegExp(req.params.codigo_ambito, "i");
      return Asignacion.find({}).populate({
        path: 'ambito',
        match:{
          // codigo_ambito: {$eq: req.params.codigo_ambito}
           $or: [{ codigo_ambito: {$regex:regex} }, { descripcion_ambito: {$regex:regex} }]
      }
      }).populate({
      path: 'entregable',
      populate: {
        path : 'proyecto',
        populate: {
          path : 'periodo'
        }
      }
    }).populate({
      path: 'persona'
    })
      .exec().catch(error => {
          res.status(500).send(error);
      });
}

exports.sumadefteall = function (req, res){
      console.log('suma de ftes todos ');
      return Asignacion.aggregate([
          {
            $lookup:{
              from:"personas",
              localField:'persona',
              foreignField:'_id',
              as:'persona'
            }
          },
          {$unwind: "$persona"},
          {
            $group:
            {
              _id:'$persona',
              ftes: { $sum: "$fte"}
            }
          }]
      ).exec().catch(error => {
          res.status(500).send(error);
      });
}

exports.listarCountAmbitos = function (req, res){
      console.log('suma de ftes todos ');
      return Asignacion.aggregate([
          {
            $lookup:{
              from:"ambitos",
              localField:'ambito',
              foreignField:'_id',
              as:'ambito'
            }
          },
          {$unwind: "$ambito"},
          {
            $group:
            {
              _id:'$ambito.descripcion_ambito',
              count: { $sum: 1}
            }
          }]
      ).exec().catch(error => {
          res.status(500).send(error);
      });
}
