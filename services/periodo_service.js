var Periodo = require('../models/periodo_model');

exports.listar = function (req,res){
  return Periodo.find({estado:1}).then(
          periodos => periodos
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.listarPorDescripcion = function (req,res){
  console.log(req.params.descripcion);
  return Periodo.find({descripcion:req.params.descripcion}).then(
            periodos => periodos
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.contador = function (req,res){
  return Periodo.find({}).then(
          periodos => {
    return periodos.length;
  }).catch(error => {
          res.status(500).send(error);
      });
}

exports.crear = function (body,id,req,res){
  var periodo = new Periodo({
    anio: 	body.anio,
    descripcion: 		body.descripcion ,
    fecha_inicio: 	  body.fecha_inicio,
    fecha_fin:body.fecha_fin,
    estado:     body.estado
  });

  return periodo.save().then(
          periodoCreado => periodoCreado
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.actualizar = function (body,req,res){
  var query = {id:body._id}
  return Periodo.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          periodoActualizado => periodoActualizado
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.eliminar = function (body,req,res){
  var query = {id:body._id};
  body = {id:body.id,estado:'0'};
  return Periodo.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          periodoActualizado => periodoActualizado
      ).catch(error => {
          res.status(500).send(error);
      });
}
