var Proyecto = require('../models/proyecto_model');
var Periodo  = require('../models/periodo_model');

exports.listarConPeriodo = function (req,res){
  return Proyecto.find({}).populate({
    path: 'periodo'}).exec();
}

exports.listarPorDescripcion = function (req,res){
  console.log(req.params.descripcion);
  return Proyecto.find({}).populate({
    path: 'periodo',
    match:{ descripcion: {$eq: req.params.descripcion}}})
    .exec().catch(error => {
        res.status(500).send(error);
    });
}

exports.listarPorSdatool = function (req,res){
  var regex = new RegExp(req.params.sdatool, "i");
  return Proyecto.find({sdatool:{$regex:regex},estado:"1"}).populate({
    path: 'periodo'})
    .exec().catch(error => {
        res.status(500).send(error);
    });
}

exports.listar = function (req,res){
  return Proyecto.find({}).then(
          proyectos => proyectos
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.contador = function (req,res){
  return Proyecto.find({}).then(
          proyectos => {
    return proyectos.length;
  }).catch(error => {
          res.status(500).send(error);
      });
}

exports.crear = function (body,id,req,res){
  var proyecto = new Proyecto({
    id: 			  id,
    prioridad: 	body.prioridad,
    dominio: 		body.dominio ,
    sdatool: 	  body.sdatool,
    descripcion:body.descripcion,
    periodo: 	  body.periodo,
    pi: 	      body.pi,
    estado:     body.estado
  });

  return proyecto.save().then(
          proyectoCreado => proyectoCreado
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.actualizar = function (body,req,res){
  var query = {id:body.id}
  return Proyecto.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          proyectoActualizado => proyectoActualizado
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.eliminar = function (body,req,res){
  var query = {id:body.id};
  body = {id:body.id,estado:'0'};
  return Proyecto.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          proyectoActualizado => proyectoActualizado
      ).catch(error => {
          res.status(500).send(error);
      });
}
