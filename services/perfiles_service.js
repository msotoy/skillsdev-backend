var menus_model = require('../models/menus_model');
var perfiles_model = require('../models/perfiles_model');
var perfilesmenus_model = require('../models/perfilesmenus_model');
var perfilesusuarios_model = require('../models/perfilesusuarios_model');
var opcionesusuarios_model = require('../models/opcionesusuario_model');


var Persona = require('../models/persona_model');


const { mongoose } = require('../configurations/mongoose_config');






exports.listarPerfilPorRegistro = function (registro,req,res){
    console.log('Atendiendo listar perfil por registro'+ registro);
    var regex = new RegExp(registro, "i");
    console.log(regex);
    return perfilesusuarios_model.find({"registro":{$regex:regex}}).then(
            perfilusuarios => perfilusuarios

        ).catch(error => {

            res.status(500).send(error);
        });

  }






exports.listarMenusPorCodigoPerfil = function (codigo_perfil,req,res){
    console.log('Atendiendo menus por perfil' + codigo_perfil);
    return perfilesmenus_model.find(  {"id_perfil":codigo_perfil, activo:true}  ).populate('id_menu').sort('id_menu').then(
            perfilmenus => perfilmenus
        ).catch(error => {

            res.status(500).send(error);
        });

  }




/*

perfilesmenus_model.find({"id_perfil":codigo_perfil,activo:true},
                       function(err, perfilesmenus) {res.status(200).send(perfilesmenus) }
                     );



exports.listarMenusPorCodigoPerfil = function (codigo_perfil,req,res){
    console.log('MEnus por perfil BAck');
    console.log(codigo_perfil);

    return perfilesmenus_model.find({"id_perfil":codigo_perfil,activo:true},   ).then(
            perfilmenus => perfilmenus
        ).catch(error => {

            res.status(500).send(error);
        });
  }
*/
