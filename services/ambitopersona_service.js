var Ambitopersona = require('../models/ambitopersona_model');
var Persona = require('../models/persona_model');

exports.listarPorAmbito = function (req,res){
      var regex = new RegExp(req.params.codigo_ambito, "i");
      return Ambitopersona.find({}).populate({
        path: 'ambito',
        match:{
          // codigo_ambito: {$eq: req.params.codigo_ambito}
         $or: [{ codigo_ambito: {$regex:regex} }, { descripcion_ambito: {$regex:regex} }]
        }
    }).populate({path: 'persona'})
      .exec().catch(error => {
          res.status(500).send(error);
      });
}

exports.listarPersonaAmbito = function (req,res){
        var regex_ambito = new RegExp(req.body.ambito,"i");
        var regex_disciplina = new RegExp(req.body.disciplina,"i")
        console.log(req.params.registro);
        return Ambitopersona.aggregate([
          {
            $lookup:{
              from:"personas",
              localField:'persona',
              foreignField:'_id',
              as:'persona'
            }
          },
          {$unwind: "$persona"},
          {
            $lookup:{
              from:"ambitos",
              localField:'ambito',
              foreignField:'_id',
              as:'ambito'
            }
          },
          {$unwind: "$ambito"},
          { $match:{
               $and: [{ "persona.building_block": req.body.building_block},
                    { "persona.disciplina":{$regex:regex_disciplina} },
                    { "ambito.descripcion_ambito":{$regex:regex_ambito}  }]}
          },
          {
            $group:
            {
              _id:'$persona',
              ambitos: { $push:{ ambito:'$ambito.descripcion_ambito', peso :'$peso'}}
            }
          }]
        ).exec().catch(error => {
            res.status(500).send(error);
        });
}

exports.listarCountAmbitos = function (req,res){
        return Ambitopersona.aggregate([
          {
            $lookup:{
              from:"ambitos",
              localField:'ambito',
              foreignField:'_id',
              as:'ambito'
            }
          },
          {$unwind: "$ambito"},
          {
            $group:
            {
              _id:'$ambito.descripcion_ambito',
              count: { $sum: 1 }
            }
          }]
        ).exec().catch(error => {
            res.status(500).send(error);
        });
}

exports.listarAmbitosRegistro = function (req,res){
        var registro = new RegExp(req.params.registro,"i");
        console.log(req.params.registro);
        return Ambitopersona.aggregate([
          {
            $lookup:{
              from:"personas",
              localField:'persona',
              foreignField:'_id',
              as:'persona'
            }
          },
          {$unwind: "$persona"},
          {
            $lookup:{
              from:"ambitos",
              localField:'ambito',
              foreignField:'_id',
              as:'ambito'
            }
          },
          {$unwind: "$ambito"},
          { $match:{
               "persona.registro": registro
               }
          },
          {
            $group:
            {
              _id:'$persona',
              ambitos: { $push:{ ambito:'$ambito.descripcion_ambito', peso :'$peso'}}
            }
          }]
        ).exec().catch(error => {
            res.status(500).send(error);
        });
}
