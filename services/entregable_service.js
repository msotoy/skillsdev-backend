var Entregable = require('../models/entregable_model');
var Proyecto = require('../models/proyecto_model');

exports.listar = function (req,res){
  return Entregable.find({}).populate({
    path: 'proyecto'}).exec();
}

exports.listarPorCodigoProyecto = function (req,res){
    return Entregable.find({}).populate({
      path: 'proyecto',
      match:{ sdatool: {$eq: req.params.sdatool}},
      populate: {
        path : 'periodo'
      }
    })
      .exec().catch(error => {
          res.status(500).send(error);
      });
}

exports.contador = function (req,res){
  return Entregable.find({}).then(
          entregables => {
    return entregables.length;
  }).catch(error => {
          res.status(500).send(error);
      });
}

exports.crear = function (body,id,req,res){
  var entregable = new Entregable({
    id: 			  id,
    codigo: 	body.codigo,
    descripcion: 		body.descripcion ,
    disciplina: 	  body.disciplina,
    especialidad:body.especialidad,
    program_manager: 	  body.program_manager,
    lider_disciplina: 	      body.lider_disciplina,
    lider_tecnico: 		body.lider_tecnico ,
    equipo_final: 	  body.equipo_final,
    tipo:body.tipo,
    id_proyecto: 	  body.id_proyecto,
    estado: 	      body.estado
  });

  return entregable.save().then(
          entregableCreado => entregableCreado
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.actualizar = function (body,req,res){
  var query = {id:body.id}
  return Entregable.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          entregableActualizado => entregableActualizado
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.eliminar = function (body,req,res){
  var query = {id:body.id};
  body = {id:body.id,estado:'0'};
  return Entregable.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          entregableActualizado => entregableActualizado
      ).catch(error => {
          res.status(500).send(error);
      });
}
