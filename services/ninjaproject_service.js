var Ninjaproject = require('../models/ninjaproject_model');
const { mongoose } = require('../configurations/mongoose_config');

exports.listarPorRegistro = function (req,res){
  console.log(req.params.registro)
  return Ninjaproject.find({registro:req.params.registro}).then(
          response => response
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.crear = function (req,res){
  var ninjaproject = new Ninjaproject({
    registro: req.body.registro,
    email: req.body.email ,
    puntos: req.body.puntos,
    cinturon: req.body.cinturon,
    niveltechu: req.body.niveltechu
  });
  return ninjaproject.save().then(
          response => response
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.actualizar = function (req,res){
  var query = {_id:req.body._id}
  return Skill.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          response => response
      ).catch(error => {
          res.status(500).send(error);
      });
}

exports.eliminar = function (req,res){
  var query = {_id:req.body._id};
  body = {_id:req.body._id,estado:'0'};
  return Skill.findOneAndUpdate(query,body,{useFindAndModify: false, new:true}).then(
          response => response
      ).catch(error => {
          res.status(500).send(error);
      });
}
